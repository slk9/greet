package greet

import "fmt"

var Shark = "Lumen"

type Octopus struct {
	Name  string
	Color string
}

func (o Octopus) String() string {
	return fmt.Sprintf("Это осминог %q он имеет %s окрас", o.Name, o.Color)
}

func Hello() {
	fmt.Println("Hello, World!")

}
